# Ecommerce
A Website For Get & Manage Information From Instagram
## Pre-Installation
In Windows, MacOS or Linux we still need these packages to work with
- Docker
- Docker-compose
## Installation
Clone The Project
```bash
git clone git@git.rabiloo.net:kigeki/instagram-insights.git
cd instagram-insights
```
Switch to Develop
```bash
git checkout develop
```
Copy .env file
```bash
cp .env.example .env
```
Setup Vendor Folder Via Docker
```bash
docker run --rm --interactive --tty \
  --volume $PWD:/app \
  composer install --ignore-platform-reqs
```
Run Sail In Background & Node Modules
```bash
vendor/bin/sail up -d
vendor/bin/sail npm install
```
Setup Key, Migration & Link Storage Folder
```bash
vendor/bin/sail artisan key:generate
vendor/bin/sail artisan migrate:fresh --seed
vendor/bin/sail artisan storage:link
```
## Run
**Run Sail consecutive (In case Sail not running in background)**
```bash
vendor/bin/sail up
```
**Render app.js, app.css for Vue**
```bash
vendor/bin/sail npm run watch
```
