<?php

namespace App\Helpers;

use App\Models\UserPlan;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;

class Menu
{

    public static function disabledAllFeature(array $features, $key = 'isAllow'): array
    {
        return $features = array_map(function ($item) use ($key) {
            return array_merge($item, [
                $key => false
            ]);
        }, $features);
    }

    public static function combineFeatureByPlan(array $features, $plan, $key = 'isAllow'): array
    {
        $combineData = [];

        foreach ($features as $value) {
            $value = array_merge($value, [
                $key => $plan->{optional($value)['id']} === 1 ? true : false
            ]);
            array_push($combineData, $value);
        }

        return $combineData;
    }

    public static function findMenu($menusDept, $id)
    {
        return Arr::first(Arr::where($menusDept, function ($item) use ($id) {
            return $item['id'] === $id;
        }));
    }
}
