<?php

namespace App\ViewShares\Shared;

use App\Http\Resources\NotificationResource;
use App\Models\DeliveryNotice;
use App\ViewShares\ViewShare;

class NotificationShared implements ViewShare
{

    public static function handle(): array
    {
        $user = request()->user();

        if (is_null($user) || is_null($roles = $user->roles())) {
            return [];
        }

        $role = $roles->first();
        $notifications = DeliveryNotice::join('notice_roles', 'notice_roles.delivery_notice_id', 'delivery_notices.id')
            ->where('delivery_notices.status', 1)
            ->where('notice_roles.role_id', optional($role)->id)
            ->orderBy('delivery_notices.created_at', 'desc')
            ->limit(3)
            ->selectRaw('delivery_notices.*')
            ->get();

        return [
            'notifications' => NotificationResource::collection($notifications)
        ];
    }
}
