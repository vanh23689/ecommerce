<?php

namespace App\ViewShares\Shared;

use App\Helpers\Menu as MenuHelper;
use App\ViewShares\ViewShare;
use Illuminate\Support\Arr;

class MenuDashboardShared implements ViewShare
{
    public static function ECOMMERCE_MENU()
    {
        return [
            [
                'name' => '投稿作成',
                'id' => 'post_analysis',
                'isAllow' => true,
                'link' => route('admin.post.create'),
            ],
            [
                'name' => '投稿分析',
                'id' => 'post_analysis',
                'isAllow' => true,
                'link' => route('admin.post.index'),
            ],
            [
                'name' => 'ハッシュタグ分析',
                'id' => 'post_analysis',
                'isAllow' => true,
                'link' => route('admin.post-hashtag.index'),
            ],
            [
                'name' => '投稿数の推移',
                'id' => 'post_analysis',
                'isAllow' => true,
                'link' => route('admin.post.post-analytics'),
            ],
            [
                'name' => 'コメント分析',
                'isAllow' => true,
                'id' => 'post_analysis',
                'link' => route('admin.comment'),
            ],
            [
                'name' => 'クリエイティブ分析',
                'isAllow' => true,
                'id' => 'post_analysis',
                'link' => route('admin.creative'),
            ],
            [
                'name' => 'ストーリーズ分析',
                'id' => 'story_analysis',
                'isAllow' => true,
                'link' => route('admin.story.analysis'),
            ],
            [
                'name' => 'アカウント分析',
                'id' => 'account_analysis',
                'isAllow' => true,
                'link' => route('admin.accounts.index'),
            ],
            [
                'name' => '競合分析',
                'id' => 'co_opetition_analysis',
                'isAllow' => true,
                'link' => route('admin.compete.index'),
            ],
            [
                'name' => '広告分析',
                'id' => 'advertising_analysis',
                'isAllow' => true,
                'link' => route('admin.advertise.list')
            ],
            // [
            //     'name' => 'ストーリーズ分析',
            //     'id' => 'post_analysis',
            //     'isAllow' => true,
            //     'link' => route('admin.story.analysis'),
            // ],
            // [
            //     'name' => 'インフルエンサー分析',
            //     'id' => 'user_analysis',
            //     'isAllow' => true,
            //     'link' => '/404',
            // ],
        ];
    }

    public static function REPORT_MENU()
    {
        return [
            [
                'name' => 'レポーティング',
                'id' => 'report_analysis',
                'isAllow' => true,
                'link' => route('admin.export.report.preview'),
            ],
        ];
    }

    public static function OTHER_MENU()
    {
        return [
            [
                'name' => 'グルーピング',
                'id' => 'business_analysis',
                'isAllow' => true,
                'link' => route('admin.group-list.index'),
            ],
            [
                'name' => '投稿作成',
                'id' => 'business_analysis',
                'isAllow' => true,
                'link' => route('admin.group-post-create'),
            ],
            [
                'name' => '投稿分析',
                'id' => 'business_analysis',
                'isAllow' => true,
                'link' => route('admin.group-post-manage'),
            ],
        ];
    }

    public static function DASHBOARD_MENU()
    {
        return [
            [
                'name' => 'ダッシュボード',
                'link' => route('admin.dashboard.index'),
                'id' => 'dashboard',
            ],
        ];
    }

    public static function handle($isFlatten = false): array
    {
        $user = request()->user();

        if (is_null($user)) {
            return [];
        }

        $menus = [
            'ecommerceMenu' => static::ecommerceMenu($user),
            'reportMenu' => static::reportMenu($user),
            'otherMenu' => static::otherMenu($user),
            'dashboardMenu' => static::dashboardMenu($user),
            'accountMenu' =>  static::accountMenu($user)
        ];
        return $isFlatten ? Arr::flatten($menus, 1) : $menus;
    }

    private static function menu($menus, $user)
    {
        if ($user->hasRole('master')) {
            return $menus;
        }



        return MenuHelper::combineFeatureByPlan($menus, '');
    }

    private static function ecommerceMenu($user)
    {
        return static::menu(static::ECOMMERCE_MENU(), $user, '');
    }

    private static function reportMenu($user)
    {
        return static::menu(static::REPORT_MENU(), $user, '');
    }

    private static function otherMenu($user)
    {
        return static::menu(static::OTHER_MENU(), $user, '');
    }

    private static function dashboardMenu($user)
    {
        return static::DASHBOARD_MENU();
    }

    private static function accountMenu($user)
    {
        return static::menu(static::ACCOUNT_MENU(), $user, '');
    }

    private static function settingMenu()
    {
        return static::SETTING_MENU();
    }

}
