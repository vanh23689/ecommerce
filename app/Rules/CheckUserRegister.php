<?php

namespace App\Rules;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class CheckUserRegister implements Rule
{
    public $type;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($type = 0)
    {
        $this->type = $type;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::where('email', $value)->whereNull('deleted_at')->first();

        if (!$user || ($user->email_verified_at !== null)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Email is available for use';
    }
}
