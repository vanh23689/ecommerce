<?php

namespace App\Http\Controllers\Backend;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterRequest;

class RegisterController extends Controller
{
    public function showRegistrationForm()
    {
        if (session()->has('exist_user')) {
            return inertia('Auth/Register', [
                'exist_user' => session('exist_user')
            ]);
        }
        return inertia('Auth/Register');
    }

    protected function create(RegisterRequest $request){
        $users = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        if($users){
            return redirect()->route('admin.login');
        }else{
            return back();
        }

    }


}
