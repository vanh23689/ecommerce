<?php

namespace App\Http\Controllers\Backend\Users;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserProfileController extends Controller
{
    protected function index(){
        $user = User::find(Auth::user()->id);
        return inertia('Profile/Index', ['user' => $user]);
    }

    protected function edit(Request $request, $id){
        DB::beginTransaction();
        try {
            $user = User::findOrFail($id);
            if($request->file('profile_photo_path')){
                $file= $request->file('profile_photo_path');
                $filename= date('Ymd:H-i').$file->getClientOriginalName();
                $file-> move(public_path('public/Image'), $filename);
                $user['profile_photo_path']= $filename;
            }
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->phone = $request->input('phone');
            $user->zip_code = $request->input('zip_code');
            $user->address = $request->input('address');
            $user->address_1 = $request->input('address_1');
            $user->address_2 = $request->input('address_2');
            $user->save();
            DB::commit();
            return response()->json($user);
        } catch(\Exception $e){
            DB::rollback();

            return response()->json($e);
        }
    }

}
