<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    public function index(){
        return inertia('Dashboard');
    }
    public function destroy()
    {
        Session::flush();

        Auth::logout();

        return redirect()->route('admin.login');
    }
}
