<?php

namespace App\Http\Middleware;

use Inertia\Middleware;
use App\Helpers\ClassLoad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     * @var string
     */
    protected $rootView = 'app';


    private function composeViewShare()
    {
        return ClassLoad::loadViewShareData();
    }

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request): ?string
    {
        return parent::version($request);
    }

    /**
     * Defines the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request)
    {
        // Get permission of user login.
        // $permissions = [];
        $user = $request->user();
        return array_merge(
            parent::share($request),
            [
                'auth' => [
                    'user' => $user,
                    // 'impersonate' => app('impersonate')->isImpersonating(),
                    // 'permissions' => $permissions
                ],
            ],
            [
                'flash' => function () {
                    return [
                        'success' => Session::get('success'),
                        'error' => Session::get('error')
                    ];
                }
            ]
        );
    }
}
