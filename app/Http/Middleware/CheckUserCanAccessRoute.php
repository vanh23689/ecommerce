<?php

namespace App\Http\Middleware;

use App\Helpers\Menu;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use App\ViewShares\Shared\MenuDashboardShared;
use Illuminate\Support\Facades\Route;

class CheckUserCanAccessRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = User::findOrFail(optional(auth()->user())->id);
        if ($user->hasRole('master')) {
            return $next($request);
        }
        // account Manager
        $accountManager = [
            'id' => 'account_analysis',
            'route' => ['admin.accounts.index']
        ];
        // post manager
        $postManager = [
            'id'    => 'post_analysis',
            'route'  => [
                'admin.post.index', 'admin.post.create', 'admin.post-comment.index', 'admin.post-comment.show',
                'admin.creative', 'admin.post-hashtag.index',
                'admin.post.post-analytics', 'admin.comment', 'admin.post-analytic'
            ]
        ];
        // co_opetition manager
        $co_opetitionManager = [
            'id'    => 'co_opetition_analysis',
            'route'  => ['admin.compete.index']
        ];
        // bussiness manager
        $bussinessManager = [
            'id'   => 'business_analysis',
            'route' => [
                'admin.group-list.index', 'admin.group-list.create', 'admin.group-list.edit', 'admin.group-post-create',
                'admin.group-post-manage', 'admin.group-analysis', 'admin.post-group-analytic'
            ]
        ];
        // advertis manager
        $advertisingManager = [
            'id'    => 'advertising_analysis',
            'route'  => ['admin.post-advertisment.index', 'admin.advertise.list', 'admin.fb-ads.detail']
        ];
        // report manager
        $reportManager = [
            'id'    => 'report_analysis',
            'route'  => ['admin.report.pdf']
        ];
        // story manager
        $story_analysis = [
            'id'    => 'story_analysis',
            'route'  => ['admin.story.analysis']
        ];

        $currentRoute = Route::currentRouteName();
        $menusAccess = MenuDashboardShared::handle(true);

        switch ($currentRoute) {
            case in_array($currentRoute, $accountManager['route']):
                $canNotAccess = $this->canAssessRoute($menusAccess, $accountManager['id']);
                return $canNotAccess ? redirect('/dashboard') : $next($request);

            case in_array($currentRoute, $postManager['route']):
                $canNotAccess = $this->canAssessRoute($menusAccess, $postManager['id']);
                return $canNotAccess ? redirect('/dashboard') : $next($request);

            case in_array($currentRoute, $co_opetitionManager['route']):
                $canNotAccess = $this->canAssessRoute($menusAccess, $co_opetitionManager['id']);
                return $canNotAccess ? redirect('/dashboard') : $next($request);

            case in_array($currentRoute, $bussinessManager['route']):
                $canNotAccess = $this->canAssessRoute($menusAccess, $bussinessManager['id']);
                return $canNotAccess ? redirect('/dashboard') : $next($request);

            case in_array($currentRoute, $advertisingManager['route']):
                $canNotAccess = $this->canAssessRoute($menusAccess, $advertisingManager['id']);
                return $canNotAccess ? redirect('/dashboard') : $next($request);

            case in_array($currentRoute, $reportManager['route']):
                $canNotAccess = $this->canAssessRoute($menusAccess, $reportManager['id']);
                return $canNotAccess ? redirect('/dashboard') : $next($request);

            case in_array($currentRoute, $story_analysis['route']):
                $canNotAccess = $this->canAssessRoute($menusAccess, $story_analysis['id']);
                return $canNotAccess ? redirect('/dashboard') : $next($request);

            case $currentRoute === 'admin.notifications.index':
                return redirect('/dashboard');
        }

        return $next($request);
    }

    // check user can assess routes
    public function canAssessRoute($menusAccess, $id)
    {
        $currentMenu = Menu::findMenu($menusAccess, $id);
        return !$currentMenu || !isset($currentMenu['isAllow']) ? true : (!$currentMenu['isAllow'] ? true : false);
    }
}