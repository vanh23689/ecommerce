<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create Acc
        $super_admin = User::create([
            'last_name' => 'Rabiloo',
            'first_name' => 'Master',
            'phone' => '0987654321',
            'email' => 'master@rabiloo.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('a12345678X'),
            'remember_token' => Str::random(10),
        ]);

        $admin = User::create([
            'last_name' => 'Rabiloo',
            'first_name' => 'Admin',
            'phone' => '0987654321',
            'email' => 'admin@rabiloo.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('a12345678X'),
            'remember_token' => Str::random(10),
        ]);

        $user = User::create([
            'last_name' => 'Rabiloo',
            'first_name' => 'User',
            'phone' => '0987654321',
            'email' => 'user@rabiloo.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('a12345678X'),
            'remember_token' => Str::random(10),
        ]);

        //Add Role for Acc
        $super_admin->assignRole('super_admin');
        $admin->assignRole('admin');
        $user->assignRole('user');

        // Permission
        Permission::create(['guard_name' => 'web', 'name' => 'create_user']);
        Permission::create(['guard_name' => 'web', 'name' => 'show_user']);
        Permission::create(['guard_name' => 'web', 'name' => 'update_user']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete_user']);

        // Permission notifications
        // Permission::create(['guard_name' => 'web', 'name' => 'manage_notification']);

        //Add Permission for Role
        $role_super_admin = Role::findByName('super_admin');
        $role_super_admin->givePermissionTo([
            'create_user', 'show_user', 'update_user', 'delete_user',
            //  'manage_notification'
        ]);



        $role_admin = Role::findByName('admin');
        $role_admin->givePermissionTo([
            'create_user', 'show_user', 'update_user', 'delete_user'
        ]);
    }
}
