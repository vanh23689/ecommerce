<?php

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Backend\LoginController;
use App\Http\Controllers\Backend\RegisterController;
use App\Http\Controllers\Backend\Users\UserController;
use App\Http\Controllers\Backend\Products\ProductController;
use App\Http\Controllers\Backend\Users\UserProfileController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Authentication Routes...
Route::prefix('admin')->name('admin.')->group(function(){
    Route::middleware('guest')->group(function () {
        // Start Login
        Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
        Route::post('login', [LoginController::class, 'login'])->name('login.post');
        // End Login
        // Start Register
        Route::get('register', [RegisterController::class, 'showRegistrationForm'])->name('register');
        Route::post('register', [RegisterController::class, 'create'])->name('register.post');
        // End Register
        });
    Route::middleware('auth')->group(function(){
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
        Route::resource('users', UserController::class);
        Route::get('user_profile', [UserProfileController::class, 'index'])->name('user_profile.index');
        Route::get('user_profile/edit/{user}', [UserProfileController::class, 'edit'])->name('user_profile.edit');
        // Products
        Route::resource('products', ProductController::class);
        //Logout
        Route::get('/logout', [DashboardController::class, 'destroy'])->name('logout');
    });
});



