import { computed } from "vue";
import { useProps } from "./prop";
const useEcommerceMenu = (_) => {
    const props = useProps();
    return computed((_) => props.value?.EcommerceMenu || []);
};

const useReportMenu = (_) => {
    const props = useProps();
    return computed((_) => props.value?.reportMenu || []);
};

const useOtherMenu = (_) => {
    const props = useProps();
    return computed((_) => props.value?.otherMenu || []);
};

const useDashboardMenu = (_) => {
    const props = useProps();
    return computed((_) => props.value?.dashboardMenu[0]?.link || []);
}

const useSettingMenu = (_) => {
    const props = useProps();
    return computed((_) => props.value?.settingMenu || []);
}
export { useEcommerceMenu, useReportMenu, useOtherMenu, useDashboardMenu, useSettingMenu };
