import './bootstrap';

import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel';

//i18n
import { createI18n } from 'vue-i18n'
import messages from './Lang'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import ja from 'element-plus/es/locale/lang/ja';
//bootstrap
// import "bootstrap/dist/css/bootstrap.css"
// import "bootstrap/dist/js/bootstrap";
//Vue Toast
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
//Vuex
import { createPinia } from "pinia";
// import { store } from './Store';
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { fas } from '@fortawesome/free-solid-svg-icons'
library.add(fas);
import { fab } from '@fortawesome/free-brands-svg-icons';
library.add(fab) ;
import { far } from '@fortawesome/free-regular-svg-icons';
library.add(far);
import '../sass/app.scss';

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => require(`./Pages/${name}.vue`),
    setup({ el, app, props, plugin }) {
        const i18n = createI18n({
            locale: 'en', // set locale
            fallbackLocale: 'en',
            allowComposition: true,
            messages
          })
        const pinia = createPinia();
        const root = createApp({ render: () => h(app, props) })
            .use(plugin)
            .mixin({ methods: { route } })
            .use(ElementPlus, {
                locale: ja,
            })
            .component("fa", FontAwesomeIcon)
            .use(VueToast, {position: 'top-right'})
            .use(pinia)
            .use(i18n);
            root.mount(el);
        return root
    },
});


InertiaProgress.init({ color: '#4B5563' });
