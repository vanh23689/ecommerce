const bodyBase = 'text-base dark:bg-gray-800 dark:text-gray-100'

export const basic = {
    body: `bg-gray-50 ${bodyBase}`,
    lightBorder: 'border-gray-100',
    lightBg: 'bg-gray-100',
    aside: 'bg-gray-800 aside-scrollbars-gray',
    asideBrand: 'bg-gray-900 text-white',
    asideMenuCloseLg: 'text-white',
    asideMenuLabel: 'text-gray-400',
    asideMenuItem: 'hover:bg-gray-600 hover:bg-opacity-50',
    asideMenuItemActive: 'font-bold text-white',
    asideMenuItemInactive: 'text-gray-300',
    asideSubmenuList: 'bg-gray-700 bg-opacity-50',
    navBarItemLabel: 'text-black',
    navBarItemLabelHover: 'hover:text-blue-500',
    navBarItemLabelActiveColor: 'text-blue-600',
    navBarMenuListUpperLabel: 'bg-gray-100',
    tableTr: 'lg:hover:bg-gray-100',
    tableTrOdd: 'lg:bg-gray-50',
    overlay: 'from-gray-700 via-gray-900 to-gray-700'
}
