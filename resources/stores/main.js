import { defineStore } from 'pinia'
import * as styles from '../js/Mixins/style'
import { darkModeKey, styleKey } from '../js/Mixins/config'
import axios from 'axios'

export const useMainStore = defineStore('main', {
    state: () => ({
        // Styles
        style: '',
        bodyStyle: '',
        lightBorderStyle: '',
        lightBgStyle: '',
        asideStyle: '',
        asideBrandStyle: '',
        asideMenuCloseLgStyle: '',
        asideMenuLabelStyle: '',
        asideMenuItemStyle: '',
        asideMenuItemActiveStyle: '',
        asideMenuItemInactiveStyle: '',
        asideSubmenuListStyle: '',
        navBarItemLabelStyle: '',
        navBarItemLabelHoverStyle: '',
        navBarItemLabelActiveColorStyle: '',
        navBarMenuListUpperLabelStyle: '',
        tableTrStyle: '',
        tableTrOddStyle: '',
        overlayStyle: '',

        /* User */
        firstName: null,
        lastName: null,
        userEmail: null,
        userAddress: null,
        userCity: null,
        userCountry: null,
        userZipcode: null,
        userAvatar: null,

        /* fullScreen - fullscreen form layout (e.g. login page) */
        isFullScreen: true,

        /* Aside */
        isAsideMobileExpanded: false,
        isAsideLgActive: false,

        /* Dark mode */
        darkMode: false,

        /* Field focus with ctrl+k (to register only once) */
        isFieldFocusRegistered: false,

        /* Sample data (commonly used) */
        clients: [],
        history: []
    }),


    actions: {
        setUser(payload){
            console.log(payload);
            if(payload.name){
                this.userName = payload.name
            }
            if(payload.email){
                this.userEmail = payload.email
            }
            if(payload.avatar){
                this.userAvatar = payload.avatar
            }
        },
        setStyle (payload) {
            if (!styles[payload]) {
              return
            }

            this.style = payload

            const style = styles[payload]

            document.body.className = style.body

            if (localStorage[styleKey] !== payload) {
              localStorage.setItem(styleKey, payload)
            }

            for (const key in style) {
              this[`${key}Style`] = style[key]
            }
          },

        asideMobileToggle (payload = null) {
        const isShow = payload !== null ? payload : !this.isAsideMobileExpanded

        document.getElementById('app').classList[isShow ? 'add' : 'remove']('ml-60', 'lg:ml-0')

        document.documentElement.classList[isShow ? 'add' : 'remove']('m-clipped')

        this.isAsideMobileExpanded = isShow
        },

        asideLgToggle (payload = null) {
        this.isAsideLgActive = payload !== null ? payload : !this.isAsideLgActive
        },

        fullScreenToggle (payload) {
        this.isFullScreen = payload

        document.documentElement.classList[payload ? 'add' : 'remove']('full-screen')
        },

        setDarkMode (payload = null) {
        const value = payload !== null ? payload : !this.darkMode

        document.documentElement.classList[value ? 'add' : 'remove']('dark')

        localStorage.setItem(darkModeKey, value ? '1' : '0')

        this.darkMode = value
        },

        fetch (sampleDataKey) {
            axios
                .get(`data-sources/${sampleDataKey}.json`)
                .then(r => {
                if (r.data && r.data.data) {
                    this[sampleDataKey] = r.data.data
                }
                })
                .catch(error => {
                alert(error.message)
                })
        },

        async fetchUserProfile(sampleDataKey){
            try{
                const data = await axios.get(route('admin.user_profile.index'))
                .then(r => {
                    console.log(r);
                    // if (r.data && r.data.data) {
                    //     console.log(r.data);
                    //     this[sampleDataKey] = r.data.data
                    // }
                })
                .catch(error => {
                    alert(error.message)
                })

            }
            catch(e){

            }
        }
    }

})
